import { ResourceRef, DeepAssign, Promise, ValueTypeFilters } from 'nui-utils';

import Weakmap from 'weakmap';

const EMPTY_ARGUMENT = 'EMPTY_ARGUMENT';

const StoreValMap = new Weakmap();
const StoreListenerMap = new Weakmap();

export default NeueUI => class Store {
	constructor(raw = false) {
		StoreValMap.set(this, {});
		StoreListenerMap.set(this, []);

		this._raw = raw;
	}

	// .set('propName', propVal);
	// .set({ propName: propVal });
	set(prop, val, thenable = false) {
		const Prom = new Promise((res, rej) => {
			if (ValueTypeFilters.Function(prop)) {
				this._setWithFunc(prop, { res, rej });
			} else if (ValueTypeFilters.Object(prop)) {
				this._setWithObj(prop, { res, rej });
			} else if (ValueTypeFilters.String(prop)) {
				this._setWithObj({ [prop]: val }, { res, rej });
			} else {
				throw new TypeError(`StoreType::set() -- Unexpected type ${typeof prop}, expected Function, Object, or String as first argument.`);
			}
		}, e => NeueUI.log(['ERROR', e.stack]));
		if (thenable === true) return Prom;
		Prom.then(() => this.notify());
		return Prom;
	}

	_setWithFunc(func, { res, rej }) {
		const newState = this.get();
		const ret = func(newState);
		return this._setWithObj(newState, { res, rej });
	}

	_setWithObj(obj, { res, rej }) {
		let newData;
		const data = StoreValMap.get(this);
		if (this._raw === true) {
			newData = Object.assign(data, obj);
		} else {
			newData = DeepAssign(data, obj);
		}

		StoreValMap.set(this, newData);

		res(this.get());
	}

	get() {
		return DeepAssign({}, StoreValMap.get(this));
	}

	notify() {
		StoreListenerMap.get(this).forEach(l => l(this.get()));
	}

	when(action, cb, e) {
		NeueUI.Dispatcher.when(action).then(cb).catch((e) => { throw e; });
	}

	getInterface() {
		return {
			ref: new ResourceRef(this.ref),
			get: () => this.get(),
			subscribe: (l) => {
				const arr = StoreListenerMap.get(this);
				arr.push(l);
				StoreListenerMap.set(this, arr);
			}
		};
	}
};
