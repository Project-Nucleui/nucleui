export default NeueUI => class ActionObjectWrapper extends NeueUI.Types.Action {
	constructor(obj) {
		super();

		this._obj = obj;

		Object.keys(obj).forEach(k => Object.defineProperty(this, k, { value: obj[k].bind(this) }));
	}
};
