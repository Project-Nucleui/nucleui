export default NeueUI => class Action {
	constructor() {}

	dispatch(action, data) {
		return NeueUI.Dispatcher.resolve(action, data);
	}
};
