export default NeueUI => ({
	Config: NeueUI.Config,
	Logger: NeueUI.Logger,
	Router: NeueUI.Router,
	Dispatcher: {
		when: NeueUI.Dispatcher.when.bind(NeueUI.Dispatcher)
	},
	Types: {
		Store: NeueUI.Types.Store
	},

	register: NeueUI.register.bind(NeueUI)
});
