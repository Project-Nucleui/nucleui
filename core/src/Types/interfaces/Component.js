export default NeueUI => ({
	Config: NeueUI.Config,
	Dispatcher: {
		when: (...args) => NeueUI.Dispatcher.when(...args)
	},
	Logger: NeueUI.Logger,
	Navigate: path => NeueUI.Dispatcher.resolve('navigate', path),
	Component: NeueUI.Types.Component,
	Types: {
		Component: NeueUI.Types.Component
	},

	Actions: NeueUI.Actions,
	Stores: NeueUI.Stores,
	Views: NeueUI.Views,

	get: NeueUI.get.bind(NeueUI),
	register: NeueUI.register.bind(NeueUI)
});
