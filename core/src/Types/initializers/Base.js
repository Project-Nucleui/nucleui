const normalizeTypeName = typeName => typeName.substr(0, 1).toUpperCase() + typeName.substr(1).toLowerCase();

import { ResourceRef, ValueTypeFilters } from 'nui-utils';

export default NeueUI => class BaseResourceInitializer {
	constructor(ResourcePkg, autoInit = true) {
		NeueUI.Logger.attach(this, `resource/${ResourcePkg.ref}`);
		this._pkg = ResourcePkg;
		if (autoInit) this.init();
	}

	init() {
		const ResourcePkg = this._pkg;
		this.ref = new ResourceRef(ResourcePkg.ref);
		this.defineRef(ResourcePkg.component);
	}

	getInterface() {
		return {
			ref: this.ref,
			Component: this.Component
		};
	}

	defineRef(component) {
		Object.defineProperty(component.prototype, 'ref', {
			writable: false,
			configurable: true,
			enumerable: false,
			value: this.ref
		});
	}

	compareArrs(arr1, arr2) {
		arr1 = (arr1 || []);
		arr2 = (arr2 || []);

		arr1.sort();
		arr2.sort();

		return (arr1.length == arr2.length) && arr1.every((e, i) => String(e) == String(arr2[i]));
	}
};
