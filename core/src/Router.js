import { ResourceRef } from 'nui-utils';
import URLUtils from 'url-utils';
import WeakMap from 'weakmap';

const Routes = new WeakMap();

export default NeueUI => new (class NeueRouter {
	constructor() {
		NeueUI.Logger.attach(this, 'router');

		const defaultRoutes = {
			Default: 'pages$base:home',
			NotFound: 'pages$base:404'
		};

		Routes.set(this, defaultRoutes);

		this.generateRoutesFromMeta();

		NeueUI.Dispatcher.when('navigate').then((path) => {
			this.log(['navigate', path], this.LOG_LOUD);

			this.requestPath = path;
			this.requestRef = String(this.generateRefFromRoutePath(path));

			const { requestRef } = this;
			this.log(['Loading page for navigation', requestRef], this.LOG_DEBUG);
			this.loadPage(requestRef);
		});
	}

	loadPage(ref) {
		const pkg = NeueUI.get(ref);
		pkg.once((pkg) => {
			pkg.props = pkg.props || {};
			Object.defineProperty(pkg.props, 'location', {
				configurable: true,
				enumerable: true,
				get: () => this.location(),
				set: () => {}
			});

			NeueUI.Dispatcher.resolve('nui.router.renderable', pkg);
		}, (e) => {
			this.log(e);
			this.loadPage(Routes.get(this).NotFound);
		});
	}

	location() {
		return new URLUtils(this.requestPath || '');
	}

	route(path, ref) {
		const ResRef = new ResourceRef(ref);
		if (ResRef.type !== 'pages') {
			throw new TypeError(`Router Module: route(path, ref): Resource ref may only be type 'pages': '${ResRef.type}'`);
		}
		const routes = Routes.get(this);
		routes[path] = ResRef;
		return true;
	}

	getRoutes() {
		return Object.assign({}, Routes.get(this));
	}

	get(ref) {
		const ResRef = new ResourceRef(ref);
		if (ResRef.type !== 'pages') {
			throw new TypeError(`Router Module: get(ref): Resource ref may only be type 'pages': '${ResRef.type}'`);
		}
		const refPromise = NeueUI.Dispatcher.resolve(`nui.resource.ready.${String(ResRef).replace(/[$:]/g, '.')}`, false);
		return (refPromise === false) ? NeueUI.import(ResRef) : refPromise;
	}

	generateRoutesFromMeta() {
		const { Routes } = NeueUI.Config.Meta;

		Object.keys(Routes).forEach((ref) => {
			const r = Routes[ref];
			if (r instanceof Array) {
				r.forEach(route => this.route(route, ref));
			} else this.route(r, ref);
		});
	}

	generateRefFromRoutePath(path) {
		let ref = this.findDefinedRoute(path);
		if (ref === null) ref = path.substr(1) || '404';

		const rootRef = new ResourceRef('pages$base:404');
		return rootRef.createRelative(ref);
	}

	findDefinedRoute(path) {
		const routes = Routes.get(this);
		if (!path || path === '/') return routes.Default;
		else if (path === '/404') return routes.NotFound;

		return routes[
			Object.keys(routes).filter((k) => {
				let e = /$^/;

				if (k.substr(0, 2) === 'R$') {
					const [match, exp, flags] = k.match(/R\$\/(.*?)\/([gimy])?$/);
					e = new RegExp(`^${exp}$`, flags || '');
				} else if (k.substr(0, 1) === '/') {
					e = new RegExp(`^${k}$`);
				} else return false;

				return e.test(path);
			}).shift()
		] || null;
	}

	testPath(path, test) {
		return (new RegExp(test)).test(path);
	}
})();
