import NeueLogger from 'nui-logger';
import { ValueTypeFilters } from 'nui-utils';
import Dispatcher from 'thenable-events';

// Import Type Extendable Classes
import StoreClass from './Types/classes/Store';
import ActionClass from './Types/classes/Action';
import ComponentClass from './Types/classes/Component';

// Import Core->Type Interfaces Classes
import StoreInterface from './Types/interfaces/Store';
import ActionInterface from './Types/interfaces/Action';
import ComponentInterface from './Types/interfaces/Component';

// Import Type Initializers
import BaseInitializer from './Types/initializers/Base';
import StoreInitializer from './Types/initializers/Store';
import ActionInitializer from './Types/initializers/Action';
import ComponentInitializer from './Types/initializers/Component';

// Import Resource Manager
import ResourceManager from './Resources';

// Import Router
import NeueRouter from './Router';

export default class NeueUI {
	constructor(config = {}) {
		this.Logger = new NeueLogger({
			namespace: config.loggerNamespace || 'NeueUI',
			verbosity: config.loggerVerbosity || NeueLogger.LOG_QUIET
		});

		this.Logger.attach(this);

		const defineImmutableProp = (name, val, obj = this) => {
			Object.defineProperty(obj, name, {
				configurable: false,
				writable: false,
				enumerable: false,
				value: val
			});
		};

		defineImmutableProp('Config', Object.assign({
			defaultReadyTimeout: 2000,
			defaultServicesTimeout: 5 * 60 * 1000
		}, config));

		// Dispatcher
		defineImmutableProp('Dispatcher', new Dispatcher(void 0, this.Logger));

		// Resources Manager
		defineImmutableProp('Resources', this.use(ResourceManager));

		// Type Extendable Classes
		defineImmutableProp('Types', {});
		defineImmutableProp('Action', this.use(ActionClass), this.Types);
		defineImmutableProp('Store', this.use(StoreClass), this.Types);
		defineImmutableProp('Component', this.use(ComponentClass), this.Types);

		// Type Interfaces
		defineImmutableProp('Interfaces', {});
		defineImmutableProp('Action', this.use(ActionInterface), this.Interfaces);
		defineImmutableProp('Store', this.use(StoreInterface), this.Interfaces);
		defineImmutableProp('Component', this.use(ComponentInterface), this.Interfaces);

		// Type Initializers
		defineImmutableProp('Initializers', {});
		defineImmutableProp('Base', this.use(BaseInitializer), this.Initializers);
		defineImmutableProp('Action', this.use(ActionInitializer), this.Initializers);
		defineImmutableProp('Store', this.use(StoreInitializer), this.Initializers);
		defineImmutableProp('Component', this.use(ComponentInitializer), this.Initializers);

		// Router
		defineImmutableProp('Router', this.use(NeueRouter));
	}

	use(Middleware) {
		return ValueTypeFilters.HasPrototype(Middleware) ? new Middleware(this) : Middleware(this);
	}
}

Object.defineProperty(NeueUI, 'Logger', {
	writable: false,
	configurable: false,
	enumerable: false,
	value: NeueLogger
});
