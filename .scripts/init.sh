#!/bin/sh

for ELEMENT in utils logger babel-builder build core platform-browser platform-node simple
do
    cd $ELEMENT
    rm -rf node_modules lib
    yarn
    yarn run build
    cd ..
done
