#!/bin/bash

cd logger/ && echo -n "nui-logger: " && git status && echo ""
cd ../utils/ && echo -n "nui-utils: " && git status && echo ""
cd ../babel-builder/ && echo -n "nui-builder-babel: " && git status && echo ""
cd ../build/ && echo -n "nui-build: " && git status && echo ""
cd ../core/ && echo -n "nui-core: " && git status && echo ""
cd ../platform-browser/ && echo -n "nui-platform-browser: " && git status && echo ""
cd ../platform-node/ && echo -n "nui-platform-node: " && git status && echo ""
cd ../test-module/ && echo -n "npm-pkg-test: " && git status && echo ""
cd ../test-site/ && echo -n "test-site: " && git status && echo "" && cd ..
