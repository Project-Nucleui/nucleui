#!/bin/bash

REGISTRY=http://localhost:4873

yarn cache clean
cd logger/; rm -rf yarn.lock node_modules; yarn && npm unpublish -f --registry $REGISTRY; rm yarn.lock; npm publish --registry $REGISTRY
cd ../utils/; rm -rf yarn.lock node_modules; yarn && npm unpublish -f --registry $REGISTRY; rm yarn.lock; npm publish --registry $REGISTRY
cd ../babel-builder/; rm -rf yarn.lock node_modules; yarn && npm unpublish -f --registry $REGISTRY; rm yarn.lock; npm publish --registry $REGISTRY
cd ../build-watch/; rm -rf yarn.lock node_modules; yarn && npm unpublish -f --registry $REGISTRY; rm yarn.lock; npm publish --registry $REGISTRY
cd ../build/; rm -rf yarn.lock node_modules; yarn && npm unpublish -f --registry $REGISTRY; rm yarn.lock; npm publish --registry $REGISTRY
cd ../core/; rm -rf yarn.lock node_modules; yarn && npm unpublish -f --registry $REGISTRY; rm yarn.lock; npm publish --registry $REGISTRY
cd ../platform-browser/; rm -rf yarn.lock node_modules; yarn &&npm unpublish -f --registry $REGISTRY; rm yarn.lock; npm publish --registry $REGISTRY
cd ../platform-node/; rm -rf yarn.lock node_modules; yarn && npm unpublish -f --registry $REGISTRY; rm yarn.lock; npm publish --registry $REGISTRY
cd ../simple/; rm -rf yarn.lock node_modules; yarn && npm unpublish -f --registry $REGISTRY; rm yarn.lock; npm publish --registry $REGISTRY
cd ../test-site/
rm -rf node_modules/nui* yarn.lock && yarn
cd ..
