#!/bin/bash

yarn cache clean

pkg_version() {
	cat package.json | grep version | tr '\n' ' ' | sed -En "s/.*\"version\": \"(.*)\".*/\1/p"
}

clean() {
	rm -rf yarn.lock node_modules lib
}

commit() {
	version="v$(pkg_version)"
	verquot="\"$version\""
	git commit -a -m $verquot
	git tag $verquot
	git push && git push --tags
}

cd logger/ && clean && yarn && commit && npm publish && \
cd ../utils/ && clean && yarn && commit && npm publish && \
cd ../babel-builder/ && clean && yarn && commit && npm publish && \
cd ../nui-build-watch/ && clean && yarn && commit && npm publish && \
cd ../build/ && clean && yarn && commit && npm publish && \
cd ../core/ && clean && yarn && commit && npm publish && \
cd ../platform-browser/ && clean && yarn && commit && npm publish && \
cd ../platform-node/ && clean && yarn && commit && npm publish && \
cd ../simple/ && clean && yarn && commit && npm publish
cd ..

yarn cache clean
