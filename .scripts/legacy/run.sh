#!/bin/bash

curDir=$(pwd);

if [ $# -ne 0 ]; then
	for arg; do
		if [[ "$arg" == *\/* ]]; then
			cd $arg
		else
			cd ../$arg/
			rm -rf ~/.local/share/verdaccio/$arg/
		fi

		rm -rf node_modules yarn.lock
		yarn
		rm yarn.lock

		npm publish -f --registry http://localhost:4873/

		cd $curDir
	done

	rm -rf node_modules/nui* yarn.lock
	yarn cache clean
	yarn
fi

yarn start
