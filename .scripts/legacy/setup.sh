#!/bin/bash

colorBg="\033[38;5;233m"
colorFg="\033[36m"

echo -n -e $colorBg
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo -n "MMMMMMMMMMMMMMMMMMMMDMMMM"
echo -n -e $colorFg
echo -n "++++"
echo -n -e $colorBg
echo "DMMMMMMMMMMMMMMMMM"

echo -n -e $colorBg
echo -n "MMMMMMMMMMMMMMMM7"
echo -n -e $colorFg
echo -n "+++"
echo -n -e $colorBg
echo -n "MMMM"
echo -n -e $colorFg
echo -n "++++"
echo -n -e $colorBg
echo -n "DMMM"
echo -n -e $colorFg
echo -n "+++"
echo -n -e $colorBg
echo "DMMMMMMMMMMM"

echo -n -e $colorBg
echo -n "MMMMMMMMMMMI"
echo -n -e $colorFg
echo -n "+++++++"
echo -n -e $colorBg
echo -n "MMMD"
echo -n -e $colorFg
echo -n "++++"
echo -n -e $colorBg
echo -n "MMMMMI"
echo -n -e $colorFg
echo -n "++++++++"
echo -n -e $colorBg
echo "DMMMMM"

echo -n -e $colorBg
echo -n "MMMMMMMD"
echo -n -e $colorFg
echo -n "++++++++"
echo -n -e $colorBg
echo -n "DMMMMD"
echo -n -e $colorFg
echo -n "++++"
echo -n -e $colorBg
echo -n "MMMMMMM7"
echo -n -e $colorFg
echo -n "++++++++"
echo -n -e $colorBg
echo "DMMMM"

echo -n -e $colorBg
echo -n "MMMMD"
echo -n -e $colorFg
echo -n "++++++++"
echo -n -e $colorBg
echo -n "IMMMMMMM"
echo -n -e $colorFg
echo -n "++++"
echo -n -e $colorBg
echo -n "DMMMM8"
echo -n -e $colorFg
echo -n "++++++++"
echo -n -e $colorBg
echo "7MMMMMMM"

echo -n -e $colorBg
echo -n "MMMMMMMMNI"
echo -n -e $colorFg
echo -n "+++++"
echo -n -e $colorBg
echo -n "IMMM7"
echo -n -e $colorFg
echo -n "++++"
echo -n -e $colorBg
echo -n "MMMD"
echo -n -e $colorFg
echo -n "++++"
echo -n -e $colorBg
echo "OMMMMMMMMMMMMMM"

echo -n -e $colorBg
echo -n "MMMMMMMMMMMM"
echo -n -e $colorFg
echo -n "+++"
echo -n -e $colorBg
echo -n "MMMD"
echo -n -e $colorFg
echo -n "++++"
echo -n -e $colorBg
echo -n "MMMM"
echo -n -e $colorFg
echo -n "+++"
echo -n -e $colorBg
echo "7MMMMMMMMMMMMMMMM"

echo -n -e $colorBg
echo -n "MMMMMMMMMMMMMMMMMD"
echo -n -e $colorFg
echo -n "++++"
echo -n -e $colorBg
echo "MMMMDMMMMMMMMMMMMMMMMMMMM"

echo -n -e $colorBg
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo -n "~~~~~~~~~~~~~"
echo -n -e $colorFg
echo -n " NeueLogic (c) 2018 "
echo -n -e $colorBg
echo "~~~~~~~~~~~~~~"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo -n -e $colorFg
echo "WARNING: PROPRIETARY AND CONFIDENTIAL"
echo "DO NOT EXECUTE UNLESS EXPLICITELY AUTHORIZED"
echo "BY A NEUELOGIC APPOINTED OFFICER."
echo -n -e $colorBg
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo -n -e $colorFg
echo "If you have been given permission to continue,"
echo -e "please press <Enter> and follow the instructions.\n"
echo "OTHERWISE, PLEASE CTRL+C IMMEDIATELY TO EXIT"
echo -n -e $colorBg
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo -n -e "\033[0m"

read nullvar

echo -n -e $colorFg
echo "┌─────────────────────────────────────────────┐"
echo "│ Welcome to the team! :)                     │"
echo "│ Press <Enter> to begin install tools        │"
echo "└─────────────────────────────────────────────┘"
echo -n -e "\033[0m"

read nullvar
npm i -g verdaccio

echo -e -n "\033[1;31m"
echo "╔═════════════════════════════════════════════╗"
echo "║ [!]  READ CAREFULLY, BEFORE CONTINUING  [!] ║"
echo "╚═════════════════════════════════════════════╝"
echo -e -n "\033[1;34m"
echo "┌─────────────────────────────────────────────┐"
echo "│ Before continuing, please install and       │"
echo -n "│ run the \`"
echo -n -e "\033[4m"
echo -n "verdaccio"
echo -n -e "\033[0m\033[1;34m"
echo -n -e "\` NPM package.              │\n"
echo "├┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘"
echo -e "└ In a separate terminal instance, execute:\n"
echo -n -e "\033[0;32m"
echo "npm config set registry http://localhost:4873"
echo "verdaccio"
echo -e "\033[1;34m"
echo "┌ Minimize or (iTerm2) Bury your verdaccio window"
echo -n "└ Then, in an "
echo -e -n "\033[1;4m"
echo -n "additional"
echo -n -e "\033[1;24m"
echo -e " window execute:\n"
echo -n -e "\033[0;32m"
echo "npm login"
echo -e "\033[1;34m"
echo "┌ After, continue here by pressing <ENTER>"
echo "├─────────────────────────────────────────────┐"
echo "│ To exit setup, press <CTRL> + <C>           │"
echo "└─────────────────────────────────────────────┘"
echo -n -e "\033[0m"

read nullvar

mkdir -p nui && cd nui/

git clone git@gitlab.com:neuelogic/nui-logger.git logger
git clone git@gitlab.com:neuelogic/nui-builder-babel.git babel-builder
git clone git@gitlab.com:neuelogic/nui-utils.git utils
git clone git@gitlab.com:neuelogic/nui-build.git build
git clone git@gitlab.com:neuelogic/nui.git core
git clone git@gitlab.com:neuelogic/nui-build-watch.git build-watch
git clone git@gitlab.com:neuelogic/nui-platform-browser.git platform-browser
git clone git@gitlab.com:neuelogic/nui-platform-node.git platform-node
git clone git@gitlab.com:neuelogic/nui-simple.git simple
git clone git@gitlab.com:neuelogic/test-module.git test-module
git clone git@gitlab.com:neuelogic/test-site.git test-site

cd logger/ && yarn && yarn build && npm publish -f --registry http://localhost:4873/
cd ../utils/ && yarn && yarn build && npm publish -f --registry http://localhost:4873/
cd ../build/ && yarn && yarn build && npm publish -f --registry http://localhost:4873/
cd ../core/ && yarn && yarn build && npm publish -f --registry http://localhost:4873/
cd ../builder-babel/ && yarn && yarn build && npm publish -f --registry http://localhost:4873/
cd ../build-watch/ && yarn && yarn build && npm publish -f --registry http://localhost:4873/
cd ../platform-browser/ && yarn && yarn build && npm publish -f --registry http://localhost:4873/
cd ../platform-node/ && yarn && yarn build && npm publish -f --registry http://localhost:4873/
cd ../simple/ && yarn && yarn build && npm publish -f --registry http://localhost:4873/
cd ../test-module/ && yarn && yarn build && npm publish -f --registry http://localhost:4873/
cd ..

cp test-site/bin/run.sh ./run.sh
chmod +x run.sh

echo -n -e "\033[0;32m"
echo "╔══════════════════════════════════════╗"
echo "║  Development Environment Installed!  ║"
echo "╚══════════════════════════════════════╝"
echo "┌──────────────────────────────────────┐"
echo "│ Get Started: 'cd nui/ && ./run.sh'   │"
echo "├──────────────────────────────────────┤"
echo "│ You can also edit run.sh and comment │"
echo "│ out the modules you don't want to    │"
echo "│ build to save time during            │"
echo "│ development. Happy coding!           │"
echo "└──────────────────────────────────────┘"
echo -n -e "\033[0m"
