import os from 'os';
import path from 'path';
import Webpack from 'webpack';
import CommonsChunkPlugin from 'webpack/lib/optimize/CommonsChunkPlugin';
import WebpackConfig, { CommonConfig } from './NeueWebpackConfig.js';

export default class NeueBuilder {
	constructor(compiler) {
		compiler.Logger.attach(this, 'NeueBuilder');

		this.log('Initializing');

		this.compiler = compiler;

		this.dispatcher = this.compiler.dispatcher;

		const Config = this.Config = compiler.Config;

		this.webpackConfig = compiler.BuildOutputObject.Webpack;
		this.maps = compiler.BuildOutputObject.Maps;

		this.Commons = ['react', 'react-dom', 'nui', 'nui-utils'];

		if (Config.Target === 'all' || Config.Target === 'node') this.initServerConfig();
		if (Config.Target === 'all' || Config.Target === 'browser') this.initClientConfig();

		this.buildWebpack();
	}

	initServerConfig() {
		let FinalConfiguration;
		const maps = this.maps;
		const wpCfg = Object.assign({}, this.webpackConfig);
		const CompilerConfig = Object.assign({}, this.compiler.Config);

		CompilerConfig.BuildOutputFolder = CompilerConfig.BuildServerOutputFolder;

		try {
			wpCfg.Commons = this.Commons;
			wpCfg.CompositeEntries = Object.assign({
				app: CompilerConfig.ServerScriptPath
			}, maps);

			this.ServerConfig = WebpackConfig(
				CompilerConfig,
				wpCfg.CompositeEntries,
				[
					new CommonsChunkPlugin({
						name: 'app',
						filename: 'app.js',
						children: false,
						chunks: wpCfg.Commons,
						minChunks: wpCfg.CompositeEntries.length
					})
				]
			);

			this.ServerConfig.output.libraryTarget = 'commonjs2';


			this.ServerConfig.target = 'node';
		} catch (e) {
			this.compiler.compileError(e);
		}

		this.log(this.ServerConfig, this.LOG_LOUD);
	}

	initClientConfig() {
		let FinalConfiguration;
		const maps = this.maps;
		const wpCfg = Object.assign({}, this.webpackConfig);
		const CompilerConfig = Object.assign({}, this.compiler.Config);

		CompilerConfig.BuildOutputFolder = CompilerConfig.BuildPublicOutputFolder;

		try {
			wpCfg.Commons = this.Commons;
			wpCfg.CompositeEntries = Object.assign({
				app: [CompilerConfig.ClientScriptPath].concat(wpCfg.Commons)
			}, maps);

			this.ClientConfig = WebpackConfig(
				CompilerConfig,
				wpCfg.CompositeEntries,
				[new CommonsChunkPlugin({
					name: 'app',
					filename: 'app.js',
					children: false/* ,
					"chunks": maps,
					"minChunks": maps.length */
				})]
			);

			this.ClientConfig.target = 'web';
		} catch (e) {
			this.compiler.compileError(e);
		}

		this.log(this.ClientConfig, this.LOG_LOUD);
	}

	buildWebpack() {
		const Config = this.compiler.Config;

		this.buildCount = 0;

		try {
			if (Config.Target === 'all' || Config.Target === 'node') Webpack(this.ServerConfig, this.handleWebpackComplete.bind(this));
			if (Config.Target === 'all' || Config.Target === 'browser') Webpack(this.ClientConfig, this.handleWebpackComplete.bind(this));
		} catch (e) {
			this.log(['ERROR', 'UNCAUGHT WEBPACK ERROR']);
			this.compiler.compileError(e);
		}
	}

	handleWebpackComplete(err, stats) {
		const Config = this.compiler.Config;

		this.buildCount++;
		if (err) return this.compiler.compileError(`[webpack] ${err}`), void 0;

		if (stats.compilation.errors && stats.compilation.errors.length) {
			return stats.compilation.errors.forEach((v) => {
				this.compiler.compileError(`[webpack] ${v.name}${os.EOL}${v.message}`);
			}), void 0;
		}

		this.log(['webpack', stats.toString()], this.LOG_LOUD);

		if (Config.Target !== 'all' || (Config.Target === 'all' && this.buildCount === 2)) {
			this.log('Webpack build complete');

			this.dispatcher.resolve('NeueWebpack', true);
		}
	}
}
