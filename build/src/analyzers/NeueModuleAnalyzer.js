import glob from 'glob';
import path from 'path';
import { ResourceRef } from 'nui-utils';
import NeueResourceAnalyzer from './NeueResourceAnalyzer.js';

export default class NeueModuleAnalyzer {
	constructor(compiler, analyzer, mod) {
		compiler.Logger.attach(this, 'NeueModuleAnalyzer');

		this.analyzer = analyzer;

		Object.assign(this, mod);

		let packageFile = '';
		if (mod.name === 'base') {
			packageFile = path.join(compiler.Config.BuildContext, 'package.json');
		} else {
			packageFile = path.join(this.path, 'package.json');
		}

		// retreive package.json file from module directory and parse
		const modulePkg = require(packageFile);

		const name = mod.name;

		Object.assign(this, modulePkg);

		this.pkgName = this.name;
		this.name = name;

		this.log(`Analyzing ${this.name}`, this.LOG_LOUD);

		this.compiler = compiler;
		this.dispatcher = compiler.dispatcher;

		this.analyzeResources();
	}

	analyzeResources() {
		const Compiler = this.compiler;

		try {
			const files = this.getFiles();

			this._resDone = [];
			this._res = files.map(v => v.ref.toString());
			this.files = files.map((v) => {
				this.dispatcher.when(`NeueResourceAnalyzer.${v.ref}`).then(
					this.handleResourceAnalyzed.bind(this)
				).catch(e => this.dispatcher.reject('NeueAnalyzer', e));

				return new NeueResourceAnalyzer(Compiler, v);
			});
		} catch (e) {
			this.dispatcher.reject(`NeueModuleAnalyzer.${this.name}`, e);
		}
	}

	handleResourceAnalyzed(res) {
		const thisRes = Object.assign({}, res);

		this.analyzer.resourceList[thisRes.ref] = thisRes;

		const depTree = this.analyzer.dependencyTree;
		const resDeps = thisRes.dependencies.All.concat();
		const resTree = {};

		for (let i = 0; i < resDeps.length; i++) {
			const depRef = resDeps[i];
			if (!depTree.hasOwnProperty(depRef)) {
				depTree[depRef] = {};
			}

			resTree[depRef] = depTree[depRef];
		}

		depTree[thisRes.ref] = Object.assign((
			depTree[thisRes.ref] || {}
		), resTree);

		this._resDone.push(thisRes.ref.toString());

		this.processComplete();
	}

	processComplete() {
		const Compiler = this.compiler;

		if (!Compiler._compareArrs(
			this._res,
			this._resDone
		)) return;

		this.dispatcher.resolve(`NeueModuleAnalyzer.${this.name}`, this);
	}

	getFiles() {
		let files = [];
		const dirs = this.getDirectories();

		Object.keys(dirs).forEach((type) => {
			const opts = { cwd: dirs[type] };

			files = files.concat([].concat(
				glob.sync('**/*.js', opts),
				glob.sync('**/*.jsx', opts)
			).map((d) => {
				const refPath = d.substr(0, d.lastIndexOf('.'));

				return {
					path: path.join(opts.cwd, d),
					ref: (
						new ResourceRef(refPath)
					).build(type, this.name)
				};
			}));
		});

		return files;
	}

	getDirectories() {
		const dirs = {};

		if (this.name === 'base') {
			['actions', 'stores', 'views', 'pages'].map(v =>
				dirs[v] = path.join(this.compiler.Config.DefaultModulePath, v)
			);
		} else {
			if (!this.directories) {
				this.dispatcher.reject('NeueModuleAnalyzer', new Error(
					`Unable to determine directory structure of module: ${this.name}`
				));
				return false;
			}
			const pkgDirs = this.directories;

			for (const type in pkgDirs) {
				dirs[type] = path.join(this.path, pkgDirs[type]);
			}
		}

		return dirs;
	}
}
