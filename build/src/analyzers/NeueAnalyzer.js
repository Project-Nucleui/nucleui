import fs from 'fs';
import path from 'path';
import mkdirp from 'mkdirp';
import NeueModuleAnalyzer from './NeueModuleAnalyzer.js';

export default class NeueAnalyzer {
	constructor(compiler) {
		compiler.Logger.attach(this, 'NeueAnalyzer');
		this.log('Initializing');

		this.compiler = compiler;
		this.dispatcher = compiler.dispatcher;

		this.dispatcher.when('NeueModuleAnalyzer.*').then(
			this.handleModuleAnalyzed.bind(this)
		).catch(e => this.dispatcher.reject('NeueAnalyzer', e));

		this.dependencyTree = {};
		this.resourceList = {};
		this.peerModuleDeps = {};

		this.analyzeModules();
	}

	analyzeModules() {
		const Compiler = this.compiler;
		const Config = Compiler.Config;

		this.log('Discovering NeueUI Modules');

		this._modulesSet = false;
		this.dispatcher.when('NeueAnalyzer.ModulesSet').then(() => {
			this._modulesSet = true;
			this.processComplete();
		}).catch(e => this.dispatcher.reject('NeueAnalyzer', e));

		try {
			const mods = [{
				name: 'base',
				path: path.normalize(Config.DefaultModulePath)
			}].concat(
				Compiler.getFolders(
					Config.NodeSiteModulesPath // Retrieve all `node_modules`
				).filter(
					d => d.substr(0, 8) === 'nui-pkg-' // Only include 'nui-' packages
				).map(d => ({
					name: d.substr(8),
					path: path.join(Config.NodeSiteModulesPath, d)
				}))
			);

			this._resDone = [];
			this._res = mods.map(v => v.name);
			this.log(mods);
			this.Modules = mods.map(m => new NeueModuleAnalyzer(Compiler, this, m));
			this.dispatcher.resolve('NeueAnalyzer.ModulesSet', true);
		} catch (e) {
			this.dispatcher.reject('NeueAnalyzer', e);
		}
	}

	handleModuleAnalyzed(mod) {
		const thisMod = Object.assign({}, mod);

		this.log(`Analysis Complete for Module: ${thisMod.name}`, this.LOG_LOUD);

		this._resDone.push(thisMod.name);

		if (!thisMod.hasOwnProperty('peerDependencies')) return this.processComplete();

		const modDeps = this.peerModuleDeps;

		for (const dep in thisMod.peerDependencies) {
			modDeps[dep] = (modDeps[dep] || []);
			modDeps[dep].push(thisMod);
		}

		this.processComplete();
	}

	processComplete() {
		const Compiler = this.compiler;

		if (!Compiler._compareArrs(
			this._res,
			this._resDone
		) || this._modulesSet === false) return;

		this.dispatcher.resolve('NeueAnalyzer', this);
	}
}
