/*
	Create entryPoint configuration for resources
*/
export default class NeueResourceBuilder {
	constructor(compiler, resource) {
		compiler.Logger.attach(this, 'NeueResourceBuilder');

		this.resource = resource;
		this.compiler = compiler;
		this.dispatcher = this.compiler.dispatcher;

		this.buildEntryPointObj();
	}

	buildEntryPointObj() {
		this.log(`Configuring entry point for ${this.resource.ref}`, this.LOG_LOUD);

		const res = this.resource;

		const EntryPoints = this.EntryPoints = [];

		const ResourceList = this.compiler.BuildOutputObject.Meta.resourceList;
		const DependencyTree = this.compiler.BuildOutputObject.Meta.dependencyTree;

		const DepTree = [res.ref].concat(
			this.getKeysDeep(DependencyTree[res.ref])
		);

		for (let i = 0; i < DepTree.length; i++) {
			const resource = ResourceList[DepTree[i]];
			if (!resource) {
				this.dispatcher.reject(`NeueResourceBuilder.${this.resource.ref.module}`, new Error(
					`[${res.ref}] Missing dependency: ${DepTree[i]}`
				));
				return;
			}

			EntryPoints.push(resource.path);
		}

		this.dispatcher.resolve(`NeueResourceBuilder.${this.resource.ref.module}`, this);
	}

	getKeysDeep(obj) {
		let keys = [];
		for (const x in obj) {
			if (typeof obj[x] === 'object') {
				keys = keys.concat(
					this.getKeysDeep(obj[x])
				);
			}

			keys.push(x);
		}

		return keys;
	}
}
