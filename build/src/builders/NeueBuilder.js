import path from 'path';
import NeueModuleBuilder from './NeueModuleBuilder.js';

export default class NeueBuilder {
	constructor(compiler) {
		compiler.Logger.attach(this, 'NeueBuilder');
		this.log('Initializing');

		this.compiler = compiler;

		this.dispatcher = this.compiler.dispatcher;

		this.buildWebpackObject();
	}

	buildWebpackObject() {
		this.log('Building Webpack Configuration');

		const Compiler = this.compiler;

		this.EntryPoints = {};
		this.CompositeEntries = [];
		this.Commons = [];

		const Modules = Compiler.BuildOutputObject.Meta.Modules;

		this._mods = Modules.map((v, i) => v.name);
		this._modsDone = [];

		this.dispatcher.when('NeueModuleBuilder.*').then(
			this.handleModuleFinished.bind(this)
		);

		for (let i = 0; i < Modules.length; i++) {
			(new NeueModuleBuilder(Compiler, Modules[i]));
		}

		/*
			Grab base webpack configuration object
			Assemble NeueModuleBuilder and NeueResourceBuilder
				results into base webpack config
			Announce result once complete
		*/
	}

	handleModuleFinished(mod) {
		this.log([mod.analyzer.name, 'Module Configuration Built'], this.LOG_LOUD);

		this._modsDone.push(mod.analyzer.name);

		if (mod.hasOwnProperty('EntryPoints')) {
			const modEntryPoints = mod.EntryPoints;
			for (const i in modEntryPoints) {
				if (!modEntryPoints.hasOwnProperty(i)) continue;

				this.EntryPoints[i] = modEntryPoints[i];
			}
		}

		this.processComplete();
	}

	processComplete() {
		const Compiler = this.compiler;

		if (!Compiler._compareArrs(
			this._mods,
			this._modsDone
		)) return;

		this.log('Finished Building Webpack Configuration');

		this.compilerOptions = {
			EntryPoints: this.EntryPoints,
			CompositeEntries: this.CompositeEntries,
			Commons: this.Commons
		};

		this.dispatcher.resolve('NeueBuilder', this);
	}
}
