import fs from 'fs-extra';
import path from 'path';
import util from 'util';

import NeueClean from './NeueClean';
import NeueCompiler from './NeueCompiler';
import NeueAnalyzer from './analyzers/NeueAnalyzer';
import NeueBuilder from './builders/NeueBuilder';
import NeueWebpack from './NeueWebpack';
import NeueRouteAggregator from './NeueRouteAggregator';
import NeueResourceMapGenerator from './NeueResourceMapGenerator';
import * as WorkerDomain from 'domain';

const SRC_REL_PATH = './src/';

export default (buildConfig, handleBefore) => {
	if (BUILD_ARGS.loglevel.toUpperCase() === 'DEBUG') {
		console.log('>>> BUILD CONFIG');
		Object.keys(buildConfig).forEach(k => console.log(k, buildConfig[k]));
		console.log('>>> END BUILD CONFIG');
	}

	const Compiler = new NeueCompiler(buildConfig);
	const { Config } = Compiler;

	Compiler.dispatcher.resolve('BeforeSetup', { Compiler, Config });

	Compiler.dispatcher.when('BuildStart').then(() => Compiler.use(NeueAnalyzer));
	Compiler.dispatcher.when('NeueAnalyzer').then((analysis) => {
		Compiler.BuildOutputObject.Meta = analysis;

		Compiler.use(NeueBuilder);
	});

	Compiler.dispatcher.when('NeueBuilder').then((builder) => {
		Compiler.BuildOutputObject.Webpack = builder.compilerOptions;

		Compiler.use(NeueRouteAggregator);
	});

	Compiler.dispatcher.when('NeueRouteAggregator').then((Routes) => {
		Compiler.BuildOutputObject.Meta.Routes = Routes;
		const { MetaOutputFolder } = Config;
		const { Meta } = Compiler.BuildOutputObject;

		fs.mkdirp(MetaOutputFolder, () => {
			// Write main Meta object
			fs.writeFile(
				path.join(MetaOutputFolder, 'meta.json'),
				JSON.stringify(Compiler.BuildOutputObject.Meta, (k, v) => (['compiler', 'dispatcher', 'analyzer'].indexOf(k) > -1 ? void 0 : v)), (err) => {
					if (err) return Compiler.dispatcher.reject('NeueMetaWrite', err);
					// Write Config object
					fs.writeFile(
						path.join(MetaOutputFolder, 'config.json'),
						JSON.stringify(Config),
						(err) => {
							if (err) return Compiler.dispatcher.reject('NeueMetaWrite', err);
							Compiler.dispatcher.resolve('NeueMetaWrite', true);
						}
					);
				}
			);
		});
	});

	Compiler.dispatcher.when('NeueMetaWrite').then(() => Compiler.use(NeueResourceMapGenerator));
	Compiler.dispatcher.when('NeueResourceMapGenerator').then((ResMapGen) => {
		Compiler.BuildOutputObject.Maps = ResMapGen.Maps;
		Compiler.use(NeueWebpack);
	});
	Compiler.dispatcher.when('NeueWebpack').then(() => {
		Compiler.log('Build complete.');
		Compiler.dispatcher.resolve('PostBuild', { Compiler, Config });
	});

	Compiler.init = (clean, dump, buildAfterClean = true) => {
		Compiler.dispatcher.resolve('BeforeBuildStart', { Compiler, Config });
		if (clean === true) {
			Compiler.use(NeueClean);
			Compiler.dispatcher.when('NeueClean').then(() => {
				if (buildAfterClean) {
					Compiler.dispatcher.resolve('BuildStart', { Compiler, Config });
				}
			});
		} else {
			Compiler.dispatcher.resolve('BuildStart', { Compiler, Config });
		}

		if (dump === true) {
			Compiler.log(util.inspect(Compiler, false, null), -1);
		}
	};

	return Compiler;
};
