export default (NeueUI) => {
	Object.defineProperty(NeueUI, 'Services', {
		value: new (class ClientServices {
			constructor() {
				NeueUI.Logger.attach(this, 'BrowserServices');
				this.log('Initializing');
			}

			get(endpoint, body, headers) {
				return this.call({ method: 'GET', endpoint, body }, headers);
			}
			post(endpoint, body, headers) {
				return this.call({ method: 'POST', endpoint, body }, headers);
			}
			put(endpoint, body, headers) {
				return this.call({ method: 'PUT', endpoint, body }, headers);
			}
			delete(endpoint, body, headers) {
				return this.call({ method: 'DELETE', endpoint, body }, headers);
			}

			call({ method, endpoint, body }, headerArg = {}) {
				const headerOptions = { ...headerArg };

				if (
					!('Content-Type' in headerOptions) &&
					typeof body === 'object'
				) {
					headerOptions['Content-Type'] = 'application/json';
				}

				const headers = new Headers({ ...headerArg });

				const req = new Request(`/rest/${endpoint}`, {
					method: method || 'POST',
					body: JSON.stringify(body || ''),
					headers
				});

				return fetch(req).then((res) => {
					if (res.ok) return res.json();
					throw new Error('Network response was not ok.');
				}).catch((err) => {
					this.log(['ERROR', 'Error calling service', err]);
					this.log(['ERROR', 'Error details:', req], this.LOG_DEBUG);
					return err;
				});
			}
		})(),
		enumerable: false,
		configurable: false,
		writable: false
	});

	return NeueUI.Services;
};
