import { ResourceRef } from 'nui-utils';

const CacheMap = new WeakMap();

export default NeueUI => new (class BrowserLoader {
	constructor() {
		NeueUI.Logger.attach(this, 'browser-loader'); // Attach logger

		CacheMap.set(this, []);

		this.dispatcher = NeueUI.dispatcher;

		this.log('Started loader', this.LOG_LOUD);

		this.init();
	}

	init() {
		const Loader = this;
		const fns = [window.webpackJsonp];
		Object.defineProperty(window, 'webpackJsonp', {
			enumerable: false,
			configurabe: false,
			set: fn => fns.push(fn),
			get: () => function webpackJsonp(init, modules, targets) {
				const args = [
					init,
					modules,
					/* eslint-disable no-param-reassign */
					targets.map((v) => {
						const fn = modules[v];
						modules[v] = (module, exports, req) => {
							fn(module, exports, req);
							module.exports(NeueUI);
						};
						return v;
					})
					/* eslint-enable no-param-reassign */
				];

				return fns.reduce((r, fn) => {
					try {
						return fn(...args);
					} catch (e) {
						Loader.log(e);
						throw e;
					}
				}, null);
			}
		});

		NeueUI.Dispatcher.when('nui.resource.import').then(ref => this.import(ref));
	}

	import(ref) {
		const resRef = new ResourceRef(ref);

		const Cache = CacheMap.get(this);
		if (Cache.indexOf(String(resRef)) > -1) return Promise.reject();
		Cache.push(String(resRef));

		this.log(`Importing ${resRef}`, this.LOG_LOUD);

		try {
			const refPath = `${NeueUI.Config.appContext}compiled.${resRef.serialize()}.js`;

			this.log([resRef.toString(), `Creating import script: '${refPath}'`], this.LOG_DEBUG);

			const scrpt = document.createElement('script');

			scrpt.setAttribute('src', refPath);

			scrpt.addEventListener('error', () => {
				NeueUI.Dispatcher.resolve(`nui.resource.404.${String(resRef).replace(/[$:]/g, '.')}`, resRef);
			});

			document.body.appendChild(scrpt);

			return NeueUI.Dispatcher.when(`nui.resource.ready.${String(resRef).replace(/[$:]/g, '.')}`);
		} catch (e) {
			this.log(['ERROR', e.stack]);
			return Promise.reject(e);
		}
	}
})();
