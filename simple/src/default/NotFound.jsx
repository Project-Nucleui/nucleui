import React from 'react';

export const Routes = 'NotFound';

export default NeueUI => class NotFoundPage extends NeueUI.Component {
	render() {
		return (
			<div>
				<h1>404 - Not Found</h1>
				<p>
					Unfortunately, the page you&apos;re looking for does not exist.
				</p>
			</div>
		);
	}
};
